# Quickstart Guide: Run your MRI Sequence using MaRCoS

> This page is telling you how to run your MRI experiment using the MaRCoS on the RedPitaya and a linux host computer.
> 
> For more details about the MaRCoS system, please refer to the official [Wiki](https://github.com/vnegnev/marcos_extras/wiki) or [this publication](http://arxiv.org/pdf/2208.01616.pdf)
> 
> The following instructions are written for AlmaLinux and this network topology:
> 
> ![Network Topology](Images/OSII1_Network_PTB.png)
> 
> However, most of the manual can be adapted for any other network and Linux host, just replace your IP adresses.

# 0. First Setup

This is only necessary to do once.
And then you have it all installed. 

- install Python 3.7 or higher
- flash the SD card with the prepared RedPitConsole image and insert it into the RedPitaya:
	- the image is linked in the respective [release description](https://gitlab.com/osii/console/redpitconsole/-/releases/) under `Software`
	- The image file ends with .direct, do not unzip etc this file, just open it with <https://etcher.balena.io/> or <https://rufus.ie/de/>
		- **Note for Etcher:** Flash from file --> More Options --> Format: All (it might not be recognized as an OS image)
- Power on RedPitaya. A red LED should be blinking in a heartbeat pattern.
- install msgpack manually:
     ```bash
    conda install -c conda-forge msgpack-python
     ```
	- **Note:** Some institutes use webproxies. If this applies to your network, make sure that you have set your webproxy correctly. For me it is `http://webproxy.berlin.ptb.de:8080`.\
		for further reading about conda and web proxy settings see [here](https://docs.anaconda.com/free/anaconda/configurations/proxy/)
	- for pip it is
		```bash
    	pip install msgpack --proxy http://webproxy.berlin.ptb.de:8080
	    ```
- create a working directory, browse in your command line somewhere reasonable and then
    ```bash
    mkdir marcos
    cd marcos
    ```
- clone the following repositories
    ```bash
    git clone https://github.com/vnegnev/marcos_client.git
    git clone https://github.com/vnegnev/marcos_extras.git
    ```
- local configs:
    - In the `marcos_client` directory copy `local_config.py.example` and rename the copied file to `locale_config.py`.
    - change or uncomment it according to your setup.\
		For me this is:
        ```python
        ## IP address: RP address or 'localhost' if emulating a local server.
        ## Uncomment one of the lines below.
        #ip_address = "localhost"
        ip_address = "192.168.0.100"

        ## Port: always 11111 for now
        port = 11111

        ## FPGA clock frequency: uncomment one of the below     to configure various
        ## system behaviour. Right now only 122.88 is   supported.
        fpga_clk_freq_MHz = 122.88 # RP-122
        #fpga_clk_freq_MHz = 125.0 # RP-125

        ## Gradient board: uncomment one of the     below to    configure the gradient data    format
        #grad_board = "gpa-fhdo"
        grad_board = "ocra1"

        ## GPA-FHDO current per volt setting    (determined     by resistors)
        gpa_fhdo_current_per_volt = 2.5

        ## Flocra-pulseq path, for use of the       flocra-pulseq library (optional).
        ## Uncomment the lines below and adjust     the path    to suit your
        ## flocra-pulseq location.
        #import sys
        #sys.path.append('/home/vlad/Documents/ mri/    flocra-pulseq')
        ```
- make sure that you have access to your RedPitaya:
    ```bash
    ping 192.168.0.100
    ```
    and
    ```bash
    ssh root@192.168.0.100
    ```
	- The first time you try to connect via ssh, you should get a warning about the host’s authenticity - just type `yes` to proceed if you’re confident you’re not going to be hacked!
	- **Note:** If you have flashed a new SD card, your host computer will refuse to ssh-connect to the RedPitaya because it has a new arbitrary fingerprint. In that case use 
        ```bash
        ssh-keygen -R 192.168.0.100
        ```
        to remove the old fingerprint. Then try again to connect via ssh.\
		And again: Type `yes` if you´re confident not being hacked when you get a warning about the host´s authenticity.
- install marcos_server:
    ```bash
    cd marcos_extras
    ./marcos_setup.sh 192.168.0.100 rp-122
    ```
	> ✅ a blue light should flash on the RedPitaya!
	> 
    > The blue light indicates that the bitstream is succesfully copied to FPGA.

# 1. Copy Bitstream to FPGA

This step is needed _everytime_ after rebooting the RedPitaya.\
You can skip copy bitstream when you just followed the instructions for the [First Setup](#0-first-setup).

From within the cloned [marcos_extras repository](https://github.com/vnegnev/marcos_extras) (in my setup this is: `home/marcos/marcos_extras/`) run the following command to copy the bitstream to the FPGA (and remember to replace the IP with your SDRLab’s IP address):

```bash
./copy_bitstream.sh 192.168.0.100 rp-122
```

> ✅ A blue light should on the RedPitaya!
> 
> ⚠️ If that’s not working, you can try to flash the SD card again and/or install the server again, see [First Setup](#0-first-setup). Otherwise, please refer to the [official MaRCoS wiki](https://github.com/vnegnev/marcos_extras/wiki/guide_setting_marcos_up#16a-installing-the-latest-fpga-firmware-and-server-linux-method)

# 2. Start the MaRCoS Server

**Only start the server, if a bitstream is already copied to FPGA!**\
Otherwise it will crash and you lose the communication to the RedPitaya.
In that case you would be forced to turn off the power of the RedPitaya and flash the SD card again due to possible corruption.

1. Log in to your SDRLab using ssh:   
    ```bash
    ssh root@192.168.0.100
    ```
2. Start the Marcos server:    
    ```bash
    ./marcos_server
    ```
3. Optional: Test whether the server is running correctly:
	1. (if not done already) clone the [marcos_client repository] [https://github.com/vnegnev/marcos_client](https://github.com/vnegnev/marcos_client)
	2. from within the cloned repository (in my setup this is: `home/marcos/marcos_client/`) execute `test_server.py`:
		```bash
        python test_server.py
        ```
		- Additonal packages like `numpy` and `matplotlib` might be neccessary to install, see error messages.

# 3. Operate

## 3.a in GUI

The [PhysioMRI_GUI](https://github.com/yvives/PhysioMRI_GUI) allows you to parameterize and start your sequences:

- just run `FirstMRI.py` in the corresponding folder.
- Don´t change anything on the little welcome window, just click `Open GUI`. 

## 3.b using Python scripts

Of course, you're also free to code your own sequence instead of using the GUI.

- Examples can be found [here](https://github.com/vnegnev/marcos_client/blob/master/examples.py).
- A Tutorial has been published [here](https://github.com/vnegnev/marcos_extras/wiki/tut_write_simple_sequence)

# 4. Shut Down

Please **ALWAYS** use `poweroff` to prevent SD card corruption.
Thanks.

```bash
ssh root@192.168.0.100 “poweroff”
```

# Troubleshooting

## Halting running experiment

see [guide_halting_running_experiment · vnegnev/marcos_extras Wiki](https://github.com/vnegnev/marcos_extras/wiki/guide_halting_running_experiment)

## Restart MaRCoS-Server

Sometimes the GUI is messing up with the marcos_server.

In that case: Kill the GUI, restart the marcos_server and then run the GUI again.

1. login to RedPitaya via ssh:
 	```bash
    ssh root@192.168.0.100
    ```
2. kill all running instances of marcos_server:
 	```bash
    killall marcos_server
    ```
3. Start a new instance of marcos_server. **Never run multiple instances of marcos_server at the same time!**
	```bash
    ./marcos_server
    ```
4. Optional: Test whether or not the server is running correctly:\
	from within the cloned marcos_client repository (in my setup this is: `home/marcos/marcos_client/`) execute `test_server.py`:
	```bash
    python test_server.py
    ```

## Linux is refusing to add new SSH Key after SD card was changed

If you change the SD card, some OS may refuse to connect to the same host via ssh with another key.
The most simple way to add the new key to `known_hosts` is to remove the old one:

```bash
ssh-keygen -R 192.168.0.100  # replace with your IP adress
```
