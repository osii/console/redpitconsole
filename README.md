# RedPitConsole

## Description

RedPitConsole is a RedPitaya-based open source console for low-field magnetic resonance imaging in a standard 19" 1 HU rack.

![Red-Pitaya based MRI Console](/Hardware/Images/front.jpeg)

## Specifications 

- [RedPitaya SDRlab 122-16](https://redpitaya.com/sdrlab-122-16/)
- [OCRA1 Rev003](https://zeugmatographix.org/ocra/2020/11/27/ocra1-spi-controlled-4-channel-18bit-dac-and-rf-attenutator/) by Marcus Prier (download schematics [here](https://data.stimulate.ovgu.de/f/76608ccbde02477abdc8/))
- MaRCoS by Vlad Negnevitsky (see [repositories here](https://github.com/vnegnev/) and [respective publication here](https://doi.org/10.48550/arXiv.2208.01616))
- Passive TR-Switch <!--- TODO add reference -->
- Preamps
- Casing
- Power Supply

## Software Setup

The console is intended to run with MaRCos.
Please refer to the [MaRCos Quickstart Guide](MaRCoS_QuickStartGuide) for the setup.

## Contacts

| Name         | Email               | Institution                                                  |
|--------------|---------------------|--------------------------------------------------------------|
| Jan Frintz   | jan.frintz@ptb.de   | Physikalisch-Technische Bundesanstalt (PTB), Berlin, Germany |
| Lukas Winter | lukas.winter@ptb.de | Physikalisch-Technische Bundesanstalt (PTB), Berlin, Germany |

## Contributors (alphabetical order)

Jan Frintz,
Tobias Mohr,
Reiner Montag,
David Schote,
Frank Seifert,
Berk Silemek,
Lukas Winter

## License, Attribution and Liability

The content in this repository is licensed under the CERN Open Hardware Licence Version 2 - Weakly Reciprocal, please see [LICENSE](LICENSE) for details and also check the [DISCLAIMER](DISCLAIMER.pdf)

RedPitConsole uses the following open source hardware designs:

- OCRA1 by Marcus Prier, Forschungscampus STIMULATE, Otto-von-Guericke University, Magdeburg. Please refer to the following link for license details https://zeugmatographix.org/ocra/2020/11/27/ocra1-spi-controlled-4-channel-18bit-dac-and-rf-attenutator/
- The microSD extender panel mount https://www.printables.com/model/250146-microsd-extender-panel-mount was published by @MichaelOlson under CC BY 4.0 https://creativecommons.org/licenses/by/4.0/

**Note:** The RedPitaya SDRlab is proprietary hardware (only parts of the schematics are available).
