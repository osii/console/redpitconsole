# RedPitConsole | Hardware

## Bill of Materials

see [Hardware_BOM](Hardware_BOM.csv)

## Build Instructions

1. Get the Schroff case ready. Milling is needed for front and back panel, for details refer to the FreeCAD files in the [Casing folder](Casing/) or check out the export files (STEP files) in the respective [release](https://gitlab.com/osii/console/redpitconsole/-/releases/).
2. Drill the holes for the screws which will hold all the parts in place. <!--- TODO reference drawing (once done) -->
3. Solder the PowerSupply PCB, see [PowerSupply/README](PowerSupply/README.md).
4. Assemble the Schroff case, except top.
5. Mount everything as shown in the picture below. <!--- TODO reference Explosion Drawing (once done) -->\
	![Top-view of the RedPitConsole](Images/topview.jpeg)
6. Do the wiring according to the schematic shown below.\
	![Schematic](Images/schematic.png)
7. Commissioning: Before switching on and installing the software it's wise to measure the voltage of each power input of each PCB before actually setting them under voltage.
8. Mount the top plate and then continue the commissioning and installation as described in [Software/MaRCoS/MaRCoS_QuickStartGuide](/Software/MaRCoS/MaRCoS_QuickStartGuide.md).
9. Enjoy the build and your research!

Feel free to ask for help if anything is unclear. In case you have improvements, additional documents, drawings etc. please share them with us (e.g. via merge request) so we can add them to the repo.\
Thanks a lot for your help to make this console and documentation better!

## Further Reading

Information about related projects and alternative hard- and software can be found here:

- https://doi.org/10.48550/arXiv.2208.01616
- https://github.com/vnegnev/marcos_extras/wiki
- https://zeugmatographix.org/ocra/
- https://www.opensourceimaging.org/
