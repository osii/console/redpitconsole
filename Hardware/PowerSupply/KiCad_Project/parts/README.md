Accidentially licensed files from SnapEDA were used. See License.txt. We are not allowed to share it, but we can share the PCBs.

Please download the files yourself here:<br>
https://www.snapeda.com/parts/THD%2010-2411N/Traco%20Power/view-part/
https://www.snapeda.com/parts/TRN%201-2410/Traco%20Power/view-part/

Or even better build your own parts and then share it here. Thank you!
