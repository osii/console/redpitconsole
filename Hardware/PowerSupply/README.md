# PowerSupply for RedPitConsole

The PowerSupply is made in two stages:

1. Mains to 24 VDC. The 24 Volts are accessible via the banana connectors on front and back panel.\
	![24V AC/DC Converter](Images/MainsConverter.png)
2. A custom PCB is designed to deliver 5V for the RedPitaya and 3.3V, -15V and +15V for the OCRA1. It is powered by the 24 VDC mains converter (stage 1).\
	![PCB PowerSuply](Images/PowerSupply_PCB.png)

## Schematic of PCB

![Schematic of custom power supply PCB](Images/PowerSupply_PCB_schematic.png)

## Bill of Materials

see [PowerSupply_BOM.csv](PowerSupply_BOM.csv)
